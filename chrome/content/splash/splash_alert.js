Components.utils.import("resource://p4v-jsm/common.js");
var prefBrc2 = new xpc.prefBrc2;

const SPLASH_SCREEN = "splash_screen";
const SPLASH_VOICE = "splash_voice";

const SPLASH_PROPERTY_SCREEN = "extensions.vocalofx.skin_splash.screen";
const SPLASH_PROPERTY_VOICE = "extensions.vocalofx.skin_splash.voice";

function init(){
	var splash_screen = document.getElementById(SPLASH_SCREEN);
	splash_screen.addEventListener("command", observe, true);
	splash_screen.checked = (prefBrc2.getCharPref(SPLASH_PROPERTY_SCREEN) == "");

	var splash_voice = document.getElementById(SPLASH_VOICE);
	splash_voice.addEventListener("command", observe, true);
	splash_voice.checked = (prefBrc2.getCharPref(SPLASH_PROPERTY_VOICE) == "");
}

function observe(aSubject, aTopic, aData){
	var target = aSubject.target;
	if(target.id == SPLASH_SCREEN){
		var screen = target.checked ? "" : "chrome://vocalofx/content/splash/suichu.png";
		prefBrc2.setCharPref(SPLASH_PROPERTY_SCREEN, screen);
	}
	else if(target.id == SPLASH_VOICE){
		var voice = target.checked ? "" : "chrome://vocalofx/content/splash/miku_kidou.wav";
		prefBrc2.setCharPref(SPLASH_PROPERTY_VOICE, voice);
	}
}
