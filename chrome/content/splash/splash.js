const Cc = Components.classes;
const Ci = Components.interfaces;

function init(){
	var pref = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch2);

	var image = pref.getCharPref("extensions.vocalofx.skin_splash.screen");
	var width = pref.getIntPref("extensions.vocalofx.skin_splash.screen_width");
	var height = pref.getIntPref("extensions.vocalofx.skin_splash.screen_height");
	var logo_image = pref.getCharPref("extensions.vocalofx.skin_splash.screen_logo");
	var spacer_height = pref.getIntPref("extensions.vocalofx.skin_splash.screen_spacer_height");
	if(!image){
		image = "chrome://vocalofx/content/splash/eclipse-3.4.png";
		width = 455;
		height = 295;
		logo_image = "";
	}

	var screen = document.getElementById("vocalofx-splash");
	screen.style.MozAppearance = "none";
	screen.style.backgroundRepeat = "no-repeat";
	screen.style.backgroundImage = "url('" + image + "')";
	screen.width = width;
	screen.height = height;

	var logo = document.getElementById("logo");
	if(logo_image){
		logo.src = logo_image;
		logo.width = width;
		logo.height = 400 * (width / 1600);
	}

	//var header = document.getElementById("header");
	//var footer = document.getElementById("footer");
	var header = document.getElementById("header-30mac");
	var footer = document.getElementById("footer-30mac");
	header.height = spacer_height;
	footer.height = height - logo.height - header.height;

	var voice = pref.getCharPref("extensions.vocalofx.skin_splash.voice");
	if(voice){
		try{
			var sound = Cc["@mozilla.org/sound;1"].createInstance(Ci.nsISound);
			var ios = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
			var url = ios.newURI(voice, null, null);
			sound.init();
			sound.play(url);
		}
		catch(e){
			// nop
		}
	}

	if(!window.arguments){
		var timeoutID = setTimeout(function(){
			clearTimeout(timeoutID);
			window.close();
		}, 3000);
	}
}
