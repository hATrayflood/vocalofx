const Cu = Components.utils;
Cu.import("resource://p4v-jsm/common.js")

const ALERT_ADDON = "extensions.vocalofx.alert_addon.";
const ALERT_THEME = "extensions.vocalofx.skin_theme_alert";
const DEFAULT_THEME = "{972ce4c6-7e08-4474-a285-3208198ce6fd}";
var prefBrc2 = new xpc.prefBrc2;
var extMgr = new xpc.extMgr;
var installedAddons = [];
var noDefaultTheme = false;

function init(){
	var addons_box = document.getElementById("addons-box");
	for(var i = 0; i < window.arguments.length; i++){
		if(window.arguments[i] == DEFAULT_THEME){
			noDefaultTheme = true;
			document.getElementById("theme").collapsed = false;
		}
		else{
			var addon = extMgr.getItemForID(window.arguments[i]);
			addons_box.appendChild(getAddonLabel(addon.name));
			installedAddons.push(window.arguments[i]);
			document.getElementById("addon").collapsed = false;
		}
	}
}

function getAddonLabel(name){
	var label = document.createElement("label");
	label.setAttribute("style", "margin-left: 2em;");
	label.setAttribute("value", name);
	return label;
}

function setAlertOff(){
	if(noDefaultTheme){
		prefBrc2.setBoolPref(ALERT_THEME, false);
	}
	for(var i = 0; i < installedAddons.length; i++){
		prefBrc2.setBoolPref(ALERT_ADDON + installedAddons[i], false);
	}
}
