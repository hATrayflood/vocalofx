Components.utils.import("resource://p4v-jsm/common.js");
var prefSvc = new xpc.prefSvc;
var prefBrc2 = new xpc.prefBrc2;

const SPLASH_SCREEN = "splash_screen";
const SPLASH_VOICE = "splash_voice";
const USE_ALLPARET = "use_allparet";
const RANDOM_SELECT = "random_select";
const TOOL_ONSTARTUP = "tool_onstartup";
const TOOL_TO_WINDOW = "tool_opento_window";
const PIAPROGM_BUTTON = "piapro_gm.button";
const PIAPROGM_NOTINST = "piapro_gm.notinstalled";

const ENABLED_ITEMS = "extensions.enabledItems";
const ALERT_THEME = "extensions.vocalofx.skin_theme_alert";
const DEFAULT_THEME = "{972ce4c6-7e08-4474-a285-3208198ce6fd}";
const ALERT_ADDON = "extensions.vocalofx.alert_addon.";
const SKIN_PROPERTY_ICONS = "extensions.vocalofx.skin_icons.";
const TOOL_PROPERTY_STARTUP = "extensions.vocalofx.tool_onstartup";
const TOOL_PROPERTY_TO_WINDOW = "extensions.vocalofx.tool_opento_window";
const SPLASH_PROPERTY_SCREEN = "extensions.vocalofx.skin_splash.screen";
const SPLASH_PROPERTY_VOICE = "extensions.vocalofx.skin_splash.voice";
const SKIN_PROPERTY_USE_ALLPARET = "extensions.vocalofx.skin_use_allparet";
const SKIN_PROPERTY_SELECT_RANDOM = "extensions.vocalofx.skin_select_random";
const GREASEMONKEY_ID = "{e4a8a97b-f2ed-450b-b12d-ee082ba24781}";

var icons_default = 
{ "back-button":"back-button-rin"
, "forward-button":"forward-button-len"
, "back-forward-dropmarker":"back-forward-dropmarker-arrow-dn"
, "home-button":"home-button-spa"
, "bookmarks-button":"bookmarks-button-score"
, "throbber":"throbber-hachune"
, "favicon":"favicon-negi"
, "bookmark-item":"bookmark-item-negiroller"
, "sidebar-treechildren":"sidebar-treechildren-negiroller"
};

var icons_items = 
{ "back-forward-button":["back-button", "forward-button", "back-forward-dropmarker"]
, "home-button":["home-button"]
, "bookmarks-button":["bookmarks-button"]
, "favicon":["favicon"]
, "bookmark-item":["bookmark-item", "sidebar-treechildren"]
, "throbber":["throbber"]
};

function init(){
	var icons = document.getElementsByClassName("icons");
	for(var i = 0; i < icons.length; i++){
		icons[i].addEventListener("command", observe, true);
		var items = icons_items[icons[i].id];
		var item = prefBrc2.getCharPref(SKIN_PROPERTY_ICONS + items[0]);
		icons[i].checked = (item != items[0]);
	}

	var theme = document.getElementById(DEFAULT_THEME);
	theme.addEventListener("command", observe, true);
	theme.checked = prefBrc2.getBoolPref(ALERT_THEME);

	var addons = prefSvc.getBranch(ALERT_ADDON).getChildList("", {});
	for(var i = 0; i < addons.length; i++){
		var addon = document.getElementById(addons[i]);
		addon.addEventListener("command", observe, true);
		addon.checked = prefBrc2.getBoolPref(ALERT_ADDON + addons[i]);
	}

	var splash_screen = document.getElementById(SPLASH_SCREEN);
	splash_screen.addEventListener("command", observe, true);
	splash_screen.checked = (prefBrc2.getCharPref(SPLASH_PROPERTY_SCREEN) == "");

	var splash_voice = document.getElementById(SPLASH_VOICE);
	splash_voice.addEventListener("command", observe, true);
	splash_voice.checked = (prefBrc2.getCharPref(SPLASH_PROPERTY_VOICE) == "");

	var tool_onstartup = document.getElementById(TOOL_ONSTARTUP);
	tool_onstartup.addEventListener("command", observe, true);
	tool_onstartup.checked = !prefBrc2.getBoolPref(TOOL_PROPERTY_STARTUP);

	var tool_opento_window = document.getElementById(TOOL_TO_WINDOW);
	tool_opento_window.addEventListener("command", observe, true);
	tool_opento_window.checked = prefBrc2.getBoolPref(TOOL_PROPERTY_TO_WINDOW);

	var use_allparet = document.getElementById(USE_ALLPARET);
	use_allparet.addEventListener("command", observe, true);
	use_allparet.checked = prefBrc2.getBoolPref(SKIN_PROPERTY_USE_ALLPARET);

	var random_select = document.getElementById(RANDOM_SELECT);
	random_select.addEventListener("command", observe, true);
	random_select.checked = prefBrc2.getBoolPref(SKIN_PROPERTY_SELECT_RANDOM);

	var items = prefBrc2.getCharPref(ENABLED_ITEMS);
	if(items.indexOf(GREASEMONKEY_ID) == -1){
		document.getElementById(PIAPROGM_NOTINST).collapsed = false;
		document.getElementById(PIAPROGM_BUTTON).disabled = true;
	}
}

function observe(aSubject, aTopic, aData){
	var target = aSubject.target;

	if(target.id == DEFAULT_THEME){
		prefBrc2.setBoolPref(ALERT_THEME, target.checked);
	}
	else if(prefBrc2.getPrefType(ALERT_ADDON + target.id)){
		prefBrc2.setBoolPref(ALERT_ADDON + target.id, target.checked);
	}
	else if(icons_items[target.id]){
		var items = icons_items[target.id];
		for(var i = 0; i < items.length; i++){
			var item = target.checked ? icons_default[items[i]] : items[i];
			prefBrc2.setCharPref(SKIN_PROPERTY_ICONS + items[i], item);
		}
	}
	else if(target.id == SPLASH_SCREEN){
		var screen = target.checked ? "" : "chrome://vocalofx/content/splash/suichu.png";
		prefBrc2.setCharPref(SPLASH_PROPERTY_SCREEN, screen);
	}
	else if(target.id == SPLASH_VOICE){
		var voice = target.checked ? "" : "chrome://vocalofx/content/splash/miku_kidou.wav";
		prefBrc2.setCharPref(SPLASH_PROPERTY_VOICE, voice);
	}
	else if(target.id == TOOL_ONSTARTUP){
		prefBrc2.setBoolPref(TOOL_PROPERTY_STARTUP, !target.checked);
	}
	else if(target.id == TOOL_TO_WINDOW){
		prefBrc2.setBoolPref(TOOL_PROPERTY_TO_WINDOW, target.checked);
	}
	else if(target.id == USE_ALLPARET){
		prefBrc2.setBoolPref(SKIN_PROPERTY_USE_ALLPARET, target.checked);
	}
	else if(target.id == RANDOM_SELECT){
		prefBrc2.setBoolPref(SKIN_PROPERTY_SELECT_RANDOM, target.checked);
	}
}

function setCheckboxes(clazz, onoff){
	var checks = document.getElementsByClassName(clazz);
	for(var i = 0; i < checks.length; i++){
		checks[i].checked = onoff;
		checks[i].doCommand();
	}
}

function setDefault(){
	document.getElementById("icons_all_on").doCommand();
	document.getElementById("addon_all_on").doCommand();
	var other =
		[ document.getElementById("splash_screen")
		, document.getElementById("splash_voice")
		, document.getElementById("tool_onstartup")
		, document.getElementById("tool_opento_window")
		, document.getElementById("use_allparet")
		, document.getElementById("random_select")
		];
	for(var i = 0; i < other.length; i++){
		other[i].checked = false;
		other[i].doCommand();
	}
}

function openPiaprogmDialog(){
	window.openDialog("chrome://vocalofx/content/tool/piapro_gm.xul", "piapro_gm", "chrome,dependent=yes,close=yes,centerscreen", null);
}
