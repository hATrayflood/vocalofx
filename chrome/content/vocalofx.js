var vocalofx = {
	MOZ_LWTHEME_USED: "lightweightThemes.isThemeSelected",
	MOZ_LWTHEME_INSTALL: "InstallBrowserTheme",
	MOZ_LWTHEME_PREVIEW: "PreviewBrowserTheme",
	MOZ_LWTHEME_PREVIEW_END: "ResetBrowserThemePreview",
	MOZ_DEFAULT_THEME: "{972ce4c6-7e08-4474-a285-3208198ce6fd}",
	SKINPREF_OBSERVER: "extensions.vocalofx.skin_",
	SKIN_PROPERTY_ICONS: "extensions.vocalofx.skin_icons.",
	SKIN_PROPERTY_IMAGE: "extensions.vocalofx.skin_image",
	SKIN_PROPERTY_POSITION: "extensions.vocalofx.skin_position",
	SKIN_PROPERTY_PALLET: "extensions.vocalofx.skin_pallet",
	SKIN_PROPERTY_CATEGORY: "extensions.vocalofx.skin_category",
	SKIN_PROPERTY_USE_ALLPARET: "extensions.vocalofx.skin_use_allparet",
	TOOL_PROPERTY_POSITION: "extensions.vocalofx.tool.position",
	TOOL_PROPERTY_STARTUP: "extensions.vocalofx.tool.startup",
	TOOL_PROPERTY_TOOL_ONSTARTUP: "extensions.vocalofx.tool_onstartup",
	TOOL_PROPERTY_PIAPROGM_ALERT: "extensions.vocalofx.tool_piaprogm_alert",
	TOOL_PROPERTY_TO_WINDOW: "extensions.vocalofx.tool_opento_window",
	ALERT_THEME: "extensions.vocalofx.skin_theme_alert",
	ICONCLASS_TOOL: "vocalofx-tool",
	ICONCLASS_SKIN_PALLET: "vocalofx-skin-pallet",
	ICONCLASS_SKIN_IMAGE: "vocalofx-skin-image",
	ICONCLASS_SKIN_MENU: "vocalofx-skin-menu",
	TOOLDIALOG_URL: "chrome://vocalofx/content/tool/tool.xul",
	ALERTDIALOG_URL: "chrome://vocalofx/content/alert.xul",
	CHROME_TOOL: "chrome://vocalofx/content/tool/",
	CHROME_SKIN: "chrome://vocalofx/content/skin/",
	BROWSER_ONLOAD: "load",
	BROWSER_UNONLOAD: "unload",
	BROWSER_PREF_CHANGED: "nsPref:changed",
	DOMWINDOWOPENED: "domwindowopened",
	DEFAULT_CATEGORY: "piapro",
	DEFAULT_SKIN: "kittokat_piacryptonD",
	DEFAULT_PALLET: "piapro",

	openVocalofxTool: function(tool){
		var option = "chrome,dependent=yes,close=yes,minimizable=yes";
		var toolsize = this.tool_properties.getString(tool + ".size").split(",");
		option += ",width=" + toolsize[0];
		option += ",height=" + toolsize[1];
		try{
			var toolprop = this.pref.getCharPref(this.TOOL_PROPERTY_POSITION + "." + tool).split(",");
			option += ",left=" + parseInt(toolprop[0]) + ",top=" + parseInt(toolprop[1]);
		}
		catch(e){
			option += ",centerscreen";
		}
		var toolname = this.vocalofx_properties.getString("vocalofx.tool." + tool);
		try{
			toolname += this.vocalofx_properties.getString("vocalofx.tool." + tool + ".title");
		}
		catch(e){
			// nop
		}
		var dialog = window.openDialog(this.TOOLDIALOG_URL, tool, option, tool, toolname);
		dialog.focus();
	}
	,
	setVocalofxToolOpened: function(tool, onoff){
		this.setMenuitemIcon(document.getElementById(this.ICONCLASS_TOOL + "-" + tool), onoff);
	}
	,
	selectVocalofxSkin: function(skin){
		var position = this.skin_properties.getString(skin + ".position");
		var category = this.skin_properties.getString(skin + ".category");

		this.pref.setCharPref(this.SKIN_PROPERTY_IMAGE, this.CHROME_SKIN + skin + ".png");
		this.pref.setCharPref(this.SKIN_PROPERTY_POSITION, position);
		this.pref.setCharPref(this.SKIN_PROPERTY_CATEGORY, category);
	}
	,
	selectVocalofxSkinPallet: function(pallet){
		var category;
		try{
			category = this.pref.getCharPref(this.SKIN_PROPERTY_CATEGORY);
		}
		catch(e){
			this.selectVocalofxSkin(this.DEFAULT_SKIN);
			return;
		}
		this.pref.setCharPref(this.SKIN_PROPERTY_PALLET + "." + category, pallet);
		this.pref.setCharPref(this.SKIN_PROPERTY_PALLET, pallet);
	}
	,
	isLWThemeUsed: function(){
		var lwt_used;
		try{
			lwt_used = this.pref.getBoolPref(this.MOZ_LWTHEME_USED);
		}
		catch(e){
			lwt_used = false;
		}
		return lwt_used;
	}
	,
	setVocalofxSkinImage: function(){
		if(this.isLWThemeUsed() || this.lwt_preview){
			return;
		}

		var image;
		var skin;
		var category;
		try{
			image = this.pref.getCharPref(this.SKIN_PROPERTY_IMAGE);
			skin = image.substring(image.lastIndexOf("/") + 1, image.length - 4);
			category = this.skin_properties.getString(skin + ".category_sub");
		}
		catch(e){
			this.selectVocalofxSkin(this.DEFAULT_SKIN);
			return;
		}

		this.main_window.style.backgroundRepeat = "no-repeat";
		this.main_window.style.backgroundImage = "url('" + image + "')";
		this.selectMenuitemIcon(this.ICONCLASS_SKIN_IMAGE, skin);
		this.selectMenuitemIcon(this.ICONCLASS_SKIN_MENU, category);
	}
	,
	setVocalofxSkinCategory: function(){
		var category;
		var pallet;
		try{
			category = this.pref.getCharPref(this.SKIN_PROPERTY_CATEGORY);
			pallet = this.pref.getCharPref(this.SKIN_PROPERTY_PALLET + "." + category);
		}
		catch(e){
			this.selectVocalofxSkin(this.DEFAULT_SKIN);
			return;
		}

		this.pref.setCharPref(this.SKIN_PROPERTY_PALLET, pallet);
		this.setVocalofxPalletMenu();
	}
	,
	setVocalofxImagePosition: function(){
		var position;
		var position_height;
		var position_ratio;
		try{
			position = this.pref.getCharPref(this.SKIN_PROPERTY_POSITION).split(",");
			position_height = Number(position[0]);
			position_ratio = Number(position[1]);
		}
		catch(e){
			this.selectVocalofxSkin(this.DEFAULT_SKIN);
			return;
		}

		try{
			var image_top = Number(vocalofx_navigator_toolbox.height.substr(0, vocalofx_navigator_toolbox.height.length - 2));
			image_top = (image_top - position_height) / position_ratio;
			if(image_top > 0){
				image_top = 0;
			}
			this.main_window.style.backgroundPosition = "100% " + image_top + "px";
		}
		catch(e){
			// nop
		}
	}
	,
	setVocalofxSkinPallet: function(){
		var pallet;
		try{
			pallet = this.pref.getCharPref(this.SKIN_PROPERTY_PALLET);
		}
		catch(e){
			this.selectVocalofxSkinPallet(this.DEFAULT_PALLET);
			return;
		}

		this.setCSSImportRule(document, this.CHROME_SKIN + "skin.css", this.CHROME_SKIN + pallet + ".css");
		this.setSidebarVocalofxSkin();
		this.selectMenuitemIcon(this.ICONCLASS_SKIN_PALLET, pallet);
	}
	,
	setSidebarVocalofxSkin: function(){
		var pallet;
		try{
			pallet = this.pref.getCharPref(this.SKIN_PROPERTY_PALLET);
		}
		catch(e){
			this.selectVocalofxSkinPallet(this.DEFAULT_PALLET);
			return;
		}

		this.setCSSImportRule(this.sidebar.contentDocument, this.CHROME_SKIN + "skin.css", this.CHROME_SKIN + pallet + ".css");
		this.setSidebarVocalofxIconsAll();
	}
	,
	setVocalofxIconsAll: function(){
		var icons;
		try{
			icons = this.pref.getBranch(this.SKIN_PROPERTY_ICONS).getChildList("", {});
		}
		catch(e){
			return;
		}

		for(var i = 0; i < icons.length; i++){
			this.setVocalofxIcons(icons[i]);
		}
	}
	,
	setSidebarVocalofxIconsAll: function(){
		var icons;
		try{
			icons = this.pref.getBranch(this.SKIN_PROPERTY_ICONS).getChildList("", {});
		}
		catch(e){
			return;
		}

		for(var i = 0; i < icons.length; i++){
			this.setSidebarVocalofxIcons(icons[i]);
		}
	}
	,
	setVocalofxIcons: function(target){
		var icon;
		try{
			icon = this.pref.getCharPref(this.SKIN_PROPERTY_ICONS + target);
			if(icon == ""){
				throw e;
			}
		}
		catch(e){
			icon = target;
		}

		this.setCustomIcon(document, target, icon);
		this.setSidebarVocalofxIcons(target);
	}
	,
	setSidebarVocalofxIcons: function(target){
		var icon;
		try{
			icon = this.pref.getCharPref(this.SKIN_PROPERTY_ICONS + target);
			if(icon == ""){
				throw e;
			}
		}
		catch(e){
			icon = target;
		}

		this.setCustomIcon(this.sidebar.contentDocument, target, icon);
	}
	,
	setCustomIcon: function(doc, target, icon){
		this.setCSSImportRule(doc, "chrome://vocalofx/content/icons/" + target + ".css", "chrome://vocalofx-platform/content/" + icon + ".css");
	}
	,
	deleteCSSRule: function(target){
		while(target.cssRules.length){
			if(target.cssRules[0].styleSheet){
				this.deleteCSSRule(target.cssRules[0].styleSheet);
			}
			target.deleteRule(0);
		}
	}
	,
	setCSSImportRule: function(target, href, importcss){
		var styleSheets = target.styleSheets;
		for(var i = 0; i < styleSheets.length; i++){
			if(styleSheets[i].href == href){
				this.deleteCSSRule(styleSheets[i]);
				styleSheets[i].insertRule("@import url(" + importcss + ");", 0);
				break;
			}
		}
	}
	,
	setVocalofxMenu: function(){
		var skins = this.skin_properties.getString("skinmenu").split(",");
		for(var i = 0; i < skins.length; i++){
			var menuitem = document.createElement("menuitem");
			menuitem.setAttribute("id", this.ICONCLASS_SKIN_IMAGE + "-" + skins[i]);
			menuitem.setAttribute("class", "menuitem-iconic " + this.ICONCLASS_SKIN_IMAGE);
			menuitem.setAttribute("label", this.vocalofx_properties.getString("vocalofx.skin." + skins[i]));
			menuitem.setAttribute("oncommand", "vocalofx.selectVocalofxSkin('" + skins[i] + "')");

			var category = this.skin_properties.getString(skins[i] + ".category_sub");
			var skin_popup = this.getCategoryPopup(category);
			skin_popup.appendChild(menuitem);
		}

		var skin_popup_pallet = document.getElementById("vocalofx-skin-popup-pallet");
		var pallets = this.skin_properties.getString("palletmenu").split(",");
		for(var i = 0; i < pallets.length; i++){
			var menuitem = document.createElement("menuitem");
			menuitem.setAttribute("id", this.ICONCLASS_SKIN_PALLET + "-" + pallets[i]);
			menuitem.setAttribute("class", "menuitem-iconic " + this.ICONCLASS_SKIN_PALLET);
			menuitem.setAttribute("label", this.vocalofx_properties.getString("vocalofx.skin.pallet." + pallets[i]));
			menuitem.setAttribute("oncommand", "vocalofx.selectVocalofxSkinPallet('" + pallets[i] + "')");
			skin_popup_pallet.appendChild(menuitem);
		}

		var tool_onstartup = this.pref.getBoolPref(this.TOOL_PROPERTY_TOOL_ONSTARTUP);
		var tool_popup = document.getElementById("vocalofx-tool-popup");
		var tools = this.tool_properties.getString("toolmenu").split(",");
		for(var i = 0; i < tools.length; i++){
			var menuitem = document.createElement("menuitem");
			menuitem.setAttribute("id", this.ICONCLASS_TOOL + "-" + tools[i]);
			menuitem.setAttribute("class", "menuitem-iconic " + this.ICONCLASS_TOOL);
			menuitem.setAttribute("label", this.vocalofx_properties.getString("vocalofx.tool." + tools[i]));
			menuitem.setAttribute("oncommand", "vocalofx.openVocalofxTool('" + tools[i] + "')");
			tool_popup.appendChild(menuitem);
			try{
				if(tool_onstartup && this.pref.getBoolPref(this.TOOL_PROPERTY_STARTUP + "." + tools[i])){
					this.openVocalofxTool(tools[i]);
				}
			}
			catch(e){
				// nop
			}
		}
	}
	,
	getCategoryPopup: function(category){
		var menupopup = document.getElementById("vocalofx-skin-popup-" + category);
		if(menupopup == null){
			menupopup = document.createElement("menupopup");
			menupopup.setAttribute("id", "vocalofx-skin-popup-" + category);
			var menu = document.createElement("menu");
			menu.setAttribute("id", "vocalofx-skin-menu-" + category);
			menu.setAttribute("class", "menu-iconic vocalofx-skin-menu");
			menu.setAttribute("label", this.vocalofx_properties.getString("vocalofx.skin_menu." + category + ".label"));
			var skin_popup_root = document.getElementById("vocalofx-skin-popup");
			menu.appendChild(menupopup);
			skin_popup_root.appendChild(menu);
		}
		return menupopup;
	}
	,
	setMenuitemIcon: function(menuitem, onoff){
		try{
			if(onoff){
				menuitem.setAttribute("image", "chrome://vocalofx/content/icons/defaultFavicon.png");
			}
			else{
				menuitem.setAttribute("image", "");
			}
		}
		catch(e){
			// nop
		}
	}
	,
	selectMenuitemIcon: function(clazz, item){
		var menuitems = document.getElementsByClassName(clazz);
		for(var i = 0; i < menuitems.length; i++){
			this.setMenuitemIcon(menuitems[i], false);
		}
		this.setMenuitemIcon(document.getElementById(clazz + "-" + item), true);
	}
	,
	setVocalofxPalletMenu: function(){
		var category;
		try{
			category = this.pref.getCharPref(this.SKIN_PROPERTY_CATEGORY);
		}
		catch(e){
			this.selectVocalofxSkin(this.DEFAULT_SKIN);
			return;
		}

		this.menuroot.src = this.CHROME_SKIN + category + "_logo.png";

		if(category == "piapro" || this.pref.getBoolPref(this.SKIN_PROPERTY_USE_ALLPARET)){
			var skin_popup = document.getElementById("vocalofx-skin-popup-pallet");
			var oldmenu = skin_popup.getElementsByTagName("menuitem");
			for(var i = 0; i < oldmenu.length; i++){
				oldmenu[i].collapsed = false;
			}
		}
		else{
			var skin_popup = document.getElementById("vocalofx-skin-popup-pallet");
			var oldmenu = skin_popup.getElementsByTagName("menuitem");
			for(var i = 0; i < oldmenu.length; i++){
				oldmenu[i].collapsed = true;
			}
			var pallets = this.skin_properties.getString("palletmenu." + category).split(",");
			for(var i = 0; i < pallets.length; i++){
				var menuitem = document.getElementById(this.ICONCLASS_SKIN_PALLET + "-" + pallets[i]);
				menuitem.collapsed = false;
			}
		}
	}
	,
	openFromToolDialog: function(aSubject){
		if(this.pref.getBoolPref(this.TOOL_PROPERTY_TO_WINDOW)){
			return;
		}

		if(this.watcher.activeWindow.location == this.TOOLDIALOG_URL){
			var intervalID = setInterval(function(){
				try{
					var xul = aSubject.location;
					if(xul != undefined && xul != "" && xul != "chrome://browser/content/browser.xul"){
						throw xul;
					}
					var url = aSubject.gLastValidURLStr;
					if(url != undefined && url != "" && url != "about:blank"){
						clearInterval(intervalID);
						window.openNewTabWith(url);
						aSubject.close();
						window.focus();
					}
				}
				catch(e){
					clearInterval(intervalID);
				}
			}, 1);
		}
	}
	,
	openReadMe: function(){
		window.openNewTabWith("about:vocalofx");
	}
	,
	openReadMeOrEE: function(){
		try{
			var category = this.pref.getCharPref(this.SKIN_PROPERTY_CATEGORY);
			switch(category){
			case "kaito":
				window.openNewTabWith("about:icecream");
			break;
			case "meiko":
				window.openNewTabWith("about:onecup");
			break;
			default:
				this.openReadMe();
			break;
			}
		}
		catch(e){
			this.openReadMe();
		}
	}
	,
	openAboutDialog: function(){
		var config = "chrome://vocalofx/content/about.xul";
		var option = "chrome,dependent=yes,close=yes,minimizable=yes,centerscreen";
		window.openDialog(config, "vocalofx-about", option);
	}
	,
	openConfigDialog: function(){
		var config = "chrome://vocalofx/content/config.xul";
		var option = "chrome,dependent=yes,close=yes,minimizable=yes,centerscreen";
		window.openDialog(config, "vocalofx-config", option);
	}
	,
	init: function(){
		window.removeEventListener(this.BROWSER_ONLOAD, this, false);

		var Cc = Components.classes;
		var Ci = Components.interfaces;
		this.pref = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch2);
		this.watcher = Cc['@mozilla.org/embedcomp/window-watcher;1'].getService(Ci.nsIWindowWatcher);
		this.brand = document.getElementById("bundle_brand");
		this.main_window = document.getElementById("main-window");
		this.sidebar = document.getElementById("sidebar");
		this.navigator_toolbox = document.getElementById("navigator-toolbox");
		this.menuroot = document.getElementById("vocalofx-menuroot");
		this.vocalofx_properties = document.getElementById("vocalofx-properties");
		this.skin_properties = document.getElementById("vocalofx-skin-properties");
		this.tool_properties = document.getElementById("vocalofx-tool-properties");

		if(this.main_window._lightweightTheme){
			this.main_window._lightweightTheme._update_orig = this.main_window._lightweightTheme._update;
			this.main_window._lightweightTheme._update = function(aData){
				this._update_orig(aData);
				vocalofx.setVocalofxSkinImage();
			};
		}
		gBrowser.mPanelContainer.addEventListener(this.MOZ_LWTHEME_INSTALL, this, false, true);
		gBrowser.mPanelContainer.addEventListener(this.MOZ_LWTHEME_PREVIEW, this, false, true);
		gBrowser.mPanelContainer.addEventListener(this.MOZ_LWTHEME_PREVIEW_END, this, false, true);
		this.pref.addObserver(this.MOZ_LWTHEME_USED, this, false);

		this.pref.addObserver(this.SKINPREF_OBSERVER, this, false);
		this.watcher.registerNotification(this);
		vocalofx_sidebar.init(this.sidebar);
		vocalofx_navigator_toolbox.init(this.navigator_toolbox);

		this.setVocalofxMenu();
		this.setVocalofxIconsAll();
		this.setVocalofxSkinPallet();
		this.setVocalofxSkinImage();
		this.setVocalofxPalletMenu();
	}
	,
	destroy: function(){
		this.pref.removeObserver(this.MOZ_LWTHEME_USED, this, false);
		gBrowser.mPanelContainer.removeEventListener(this.MOZ_LWTHEME_INSTALL, this, false);
		gBrowser.mPanelContainer.removeEventListener(this.MOZ_LWTHEME_PREVIEW, this, false);
		gBrowser.mPanelContainer.removeEventListener(this.MOZ_LWTHEME_PREVIEW_END, this, false);
		window.removeEventListener(this.BROWSER_ONLOAD, this, false);
		this.pref.removeObserver(this.SKINPREF_OBSERVER, this, false);
		this.watcher.unregisterNotification(this);
		vocalofx_sidebar.destroy();
		vocalofx_navigator_toolbox.destroy();
	}
	,
	observe: function(aSubject, aTopic, aData){
		switch(aTopic){
		case this.BROWSER_PREF_CHANGED:
			switch(aData){
			case this.SKIN_PROPERTY_IMAGE:
				this.setVocalofxSkinImage();
			break;
			case this.SKIN_PROPERTY_POSITION:
				this.setVocalofxImagePosition();
			break;
			case this.SKIN_PROPERTY_PALLET:
				this.setVocalofxSkinPallet();
			break;
			case this.SKIN_PROPERTY_CATEGORY:
				this.setVocalofxSkinCategory();
			break;
			case this.SKIN_PROPERTY_USE_ALLPARET:
				this.setVocalofxPalletMenu();
			break;
			case this.MOZ_LWTHEME_USED:
				if(!this.isLWThemeUsed()){
					this.setVocalofxSkinImage();
				}
			break;
			}
			if(aData.indexOf(this.SKIN_PROPERTY_ICONS) == 0){
				this.setVocalofxIcons(aData.substring(this.SKIN_PROPERTY_ICONS.length));
			}
		break;
		case this.DOMWINDOWOPENED:
			this.openFromToolDialog(aSubject);
		break;
		}
	}
	,
	handleEvent: function(aEvent){
		switch(aEvent.type){
		case this.BROWSER_ONLOAD:
			this.init();
		break;
		case this.BROWSER_UNONLOAD:
			this.destroy();
		break;
		case this.MOZ_LWTHEME_INSTALL:
			if(this.pref.getBoolPref(this.ALERT_THEME)){
				var dialog = window.openDialog(this.ALERTDIALOG_URL, "_blank", "chrome,centerscreen,alwaysRaised=yes,modal=yes", this.MOZ_DEFAULT_THEME);
				dialog.focus();
			}
		break;
		case this.MOZ_LWTHEME_PREVIEW:
			this.lwt_preview = true;
		break;
		case this.MOZ_LWTHEME_PREVIEW_END:
			this.lwt_preview = false;
		break;
		}
	}
};

var vocalofx_sidebar = {
	SIDEBAR_ONLOAD: "load",

	init: function(sidebar){
		this.sidebar = sidebar;
		this.sidebar.addEventListener(this.SIDEBAR_ONLOAD, this, true);
	}
	,
	destroy: function(){
		this.sidebar.removeEventListener(this.SIDEBAR_ONLOAD, this, true);
	}
	,
	handleEvent: function(aEvent){
		switch(aEvent.type){
		case this.SIDEBAR_ONLOAD:
			vocalofx.setSidebarVocalofxSkin();
		break;
		}
	}
};

var vocalofx_navigator_toolbox = {
	NAVIGATOR_TOOLBAR_MODIFIED: "DOMAttrModified",

	init: function(navigator_toolbox){
		this.height = "0px";
		this.navigator_toolbox = navigator_toolbox;
		this.navigator_toolbox.addEventListener(this.NAVIGATOR_TOOLBAR_MODIFIED, this, true);
		this.style = document.defaultView.getComputedStyle(this.navigator_toolbox, "");
	}
	,
	destroy: function(){
		this.navigator_toolbox.removeEventListener(this.NAVIGATOR_TOOLBAR_MODIFIED, this, true);
	}
	,
	isNavigatorToolboxHeightChanged: function(){
		if(this.height != this.style.height){
			this.height = this.style.height;
			return true;
		}
		else{
			return false;
		}
	}
	,
	handleEvent: function(aEvent){
		switch(aEvent.type){
		case this.NAVIGATOR_TOOLBAR_MODIFIED:
			if(this.isNavigatorToolboxHeightChanged()){
				vocalofx.setVocalofxImagePosition();
			}
		break;
		}
	}
};

window.addEventListener(vocalofx.BROWSER_ONLOAD, vocalofx, false);
window.addEventListener(vocalofx.BROWSER_UNONLOAD, vocalofx, false);
