var vocalofx_properties;

function init(){
	var em = Components.classes["@mozilla.org/extensions/manager;1"].getService(Components.interfaces.nsIExtensionManager);
	var addon = em.getItemForID("vocalofx@rayflood.org");
	vocalofx_properties = document.getElementById("vocalofx-properties");

	var extensionName = document.getElementById("extensionName");
	extensionName.value = addon.name;
	var extensionVersion = document.getElementById("extensionVersion");
	extensionVersion.value += addon.version;
	var extensionDescription = document.getElementById("extensionDescription");
	extensionDescription.value += vocalofx_properties.getString("extensions.vocalofx@rayflood.org.description");

	var illustrators = 
		[ "tamago"
		, "saNari"
		, "nagamon"
		, "amino"
		, "mayo"
		, "torikoa666"
		, "sabared"
		, "komore"
		, "zasikiusa"
		, "asaki3103"
		, "atora"
		, "momonenami"
		, "sarahimuro"
		, "sanpati"
		, "kaya"
		, "ruchi_kk"
		, "necco1211"
		, "bsxxx5419"
		, "karuru"
		, "hachimetre"
		, "t_tomoe"
		, "rumeco"
		, "NASHI_K"
		, "megumistina"
		, "shiwasuta"
		, "nyaco"
		, "ion"
		, "kittokat"
		, "tomoyoshi"
		, "osayo"
		, "hannya2525"
		, "yuki"
		];
	var tools = 
		[ "motoda"
		, "shikaku"
		, "xaby"
		, "ezil"
		, "piaprousers"
		];
	var colorpallets = 
		[ "smallwebmemo"
		];
	var specialthanks = 
		[ "gf-tlv"
		, "yararchive"
		, "mikugermvictim"
		];
	var creators = 
		[ "h"
		];

	setSectionBox(document.getElementById("illustratorBox").childNodes, illustrators);
	setSectionBox(document.getElementById("toolBox").childNodes, tools);
	setSectionBox(document.getElementById("colorpalletBox").childNodes, colorpallets);
	setSectionBox(document.getElementById("specialthanksBox").childNodes, specialthanks);
	setSectionBox(document.getElementById("creatorBox").childNodes, creators);

	var extensionsStrings = document.getElementById("extensionsStrings");
	var acceptButton = document.documentElement.getButton("accept");
	acceptButton.label = extensionsStrings.getString("aboutWindowCloseButton");
	document.title = extensionsStrings.getFormattedString("aboutWindowTitle", [addon.name]);
}

function setSectionBox(vbox, creators){
	var homepagelabel = vocalofx_properties.getString("vocalofx.about.homepage.label");
	for(var i = 0; i < creators.length; i++){
		var name = vocalofx_properties.getString("vocalofx.creator." + creators[i] + ".name");
		var homepage = vocalofx_properties.getString("vocalofx.creator." + creators[i] + ".homepage");
		vbox[i % vbox.length].appendChild(getCreatorBox(name, homepage, homepagelabel));
	}
}

function getCreatorBox(name, homepage, homepagelabel){
	var hbox = document.createElement("hbox");
	hbox.setAttribute("class", "indent");
	var labelCreator = document.createElement("label");
	labelCreator.setAttribute("class", "contributor");
	labelCreator.setAttribute("value", name);
	labelCreator.setAttribute("flex", "1");
	labelCreator.setAttribute("crop", "right");
	var labelHomepage = document.createElement("label");
	labelHomepage.setAttribute("class", "text-link");
	labelHomepage.setAttribute("value", homepagelabel);
	labelHomepage.setAttribute("onclick", "loadHomepage(event);");
	labelHomepage.setAttribute("homepageURL", homepage);
	labelHomepage.setAttribute("tooltiptext", homepage);
	hbox.appendChild(labelCreator);
	hbox.appendChild(labelHomepage);
	return hbox;
}

function loadHomepage(aEvent){
	window.close();
	window.opener.openURL(aEvent.target.getAttribute("homepageURL"));
}

function openSplashScreen(){
	var splash = "chrome://vocalofx/content/splash/splash.xul";
	var option = "chrome,centerscreen,alwaysRaised=yes,titlebar=no,modal=yes";
	window.openDialog(splash, "vocalofx-splash", option, -1);
}
