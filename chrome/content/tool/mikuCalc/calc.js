Components.utils.import("resource://p4v-jsm/common.js");
function $(id){
    return document.getElementById(id)
}
function range(begin,end){
    for(let i=begin;i<end;i++){
	yield i;
    }
}
function debug(str){
    var con=new xpc.conSvc;
    con.logStringMessage(str)
}
function WavSound(name,length){
    this.filename=name
    this.length=length
}
WavSound.prototype={
    filename:null,
    length:0
}
var mikuCalc={
    prevNum:0,
    currentNum:0,
    operator:"+",
    lastKey:false,
    mode:null,
    memories:{
	M:0
    },
    input:function(c){
        var calcBox=$("calcBox")
	this.speak(c)
	switch(c){
	    case "AC":
	    case "DEL":
		this[c]()
		return;
	    case ".":
		if(calcBox.value.indexOf(".")!=-1) return;
		calcBox.value+=c
		return;
	    case "+/-":
		if(calcBox.value!="0"){
		    calcBox.value=-parseFloat(calcBox.value)
		    return;
		}
	    case String.fromCharCode(0):
		c="="
	    case "+":
	    case "-":
	    case "*":
	    case "/":
	    case "=":
		/*debug("p:"+this.prevNum)
		debug("c:"+this.currentNum)*/
		if(this.prevNum!=Number(calcBox.value)) this.currentNum=Number(calcBox.value)
		if(!this.lastKey){
		    this[this.operator]();
		    calcBox.value=this.prevNum
		}
		this.lastKey=true
		this.operator=c
		if(c=="="){
		    this["="]()
		}
		return;
	    case "M+":
		this.addmem()
		return;
	    case "MR":
		this.remember()
		return
	    case "MC":
		this.memClear()
		return
	    default:
		if(String(Number(c))=="NaN")return;
	}
	//debug(this.lastKey)
	if(calcBox.value=="0"||this.lastKey){
	    if(this.operator=="=") this.prevNum=c
	    calcBox.value=c
	}else calcBox.value+=c
	this.lastKey=false
    },
    /*wavtable:{
        0:"0.wav",
	1:"1.wav",
	2:"2.wav",
	3:"3.wav",
	4:"4.wav",
	5:"5.wav",
	6:"6.wav",
	7:"7.wav",
	8:"8.wav",
	9:"9.wav",
	10:"10.wav",
	100:"100.wav",
	1000:"1000.wav",
	3000:"3000.wav",
	".":"dot.wav",
	"+":"add.wav",
	"-":"substract.wav",
	"*":"multiply.wav",
	"/":"divide.wav",
	"=":"equal.wav",
	"だよ":"dayo.wav",
	"bad":"bad.wav",
	"DEL":"del.wav",
	"M+":"mplus.wav"
    },*/
    wavtable:{
        0:new WavSound("0.wav",330),
	1:new WavSound("1.wav",280),
	2:new WavSound("2.wav",210),
	3:new WavSound("3.wav",370),
	4:new WavSound("4.wav",320),
	5:new WavSound("5.wav",145),
	6:new WavSound("6.wav",300),
	7:new WavSound("7.wav",300),
	8:new WavSound("8.wav",310),
	9:new WavSound("9.wav",270),
	10:new WavSound("10.wav",340),
	100:new WavSound("100.wav",300),
	1000:new WavSound("1000.wav",400),
	3000:new WavSound("3000.wav",360),
	".":new WavSound("dot.wav",290),
	"+":new WavSound("add.wav",280),
	"-":new WavSound("substract.wav",320),
	"*":new WavSound("multiply.wav",420),
	"/":new WavSound("divide.wav",320),
	"=":new WavSound("equal.wav",320),
	"だよ":new WavSound("dayo.wav",280),
	"bad":new WavSound("bad.wav",1200),
	"DEL":new WavSound("del.wav",490),
	"M+":new WavSound("mplus.wav",750),
	"MR":new WavSound("remem2.wav",780),
	"MC":new WavSound("forget.wav",840),
	"AC":new WavSound("ac.wav",650)
    },
    buildNumArray:function(){
	var numArray=[]
	var parts=$("calcBox").value.split(".")
	var intPart=parts[0].split("")
	if(parts.length==2) var floatPart=parts[1].split("")
	var pow10=[Math.pow(10,i) for each (i in range(1, intPart.length))].reverse()
	for(let i=0;i<intPart.length;i++){
	    if(intPart[i]==0) continue;
	    numArray.push(intPart[i])
	    switch(pow10[i]){
		case undefined:break;
		case 1000:
		    if(intPart[i]==3||intPart[i]==8){
			numArray.push(intPart[i]*1000)
			break;
		    }
		default:numArray.push(pow10[i])
	    }
	}
	numArray=numArray.filter(function(e,i){
	    return (e!=1||i==numArray.length-1)
	})
	if(floatPart) numArray=numArray.concat(["."],floatPart)
	return numArray
    },
    "+":function(){
        this.prevNum=mikuCalc.prevNum+mikuCalc.currentNum
    },
    "-":function(){
        this.prevNum=this.prevNum-this.currentNum
    },
    "*":function(){
        this.prevNum=mikuCalc.prevNum*mikuCalc.currentNum
    },
    "/":function(){
        this.prevNum=mikuCalc.prevNum/mikuCalc.currentNum
    },
    "=":function(){
	var numArray=this.buildNumArray()
	numArray.push("だよ")
	//debug(numArray)
    	var i=0
	var timer=setTimeout(function(){
	    mikuCalc.speak(numArray[i])
	    i++
	    if(i==numArray.length) clearInterval(timer)
	    else{
		var timer=setTimeout(arguments.callee,mikuCalc.wavtable[numArray[i-1]].length)
	    }
	},0)
    },
    reset:function(){
	this.prevNum=0
	this.currentNum=0
	this.operator="+"
    },
    AC:function(){
	this.speak("AC")
	$("calcBox").value=0
	this.reset()
    },
    DEL:function(){
	var calcBox=$("calcBox")
	calcBox.value=calcBox.value.slice(0,-1)
	if(calcBox.value=="") calcBox.value=0
    },
    copy:function(){
	var str=new xpc.supportsString;
        str.data=$("calcBox").value;
	var xfer=new xpc.xferData;
	xfer.addDataFlavor("text/unicode");
	xfer.setTransferData("text/unicode", str, $("calcBox").value.length * 2);
	var clip=new xpc.clipboard;
	clip.setData(xfer,null,1)
    },
    paste:function(){
	var xfer=new xpc.xferData;
	var clip=new xpc.clipboard;
	xfer.addDataFlavor("text/unicode");
	clip.getData(xfer, clip.kGlobalClipboard);
	var str={}
	var strLength={}
	xfer.getTransferData("text/unicode", str, strLength);
	if (str) str=str.value.QueryInterface(Components.interfaces.nsISupportsString);
	if (str) $("calcBox").value= str.data.substring(0, strLength.value / 2); 
    },
    speak:function(c){
	if(!this.wavtable[c]) return;
	//debug("speak():"+c)
	var ios=new xpc.ioSvc;
	var snd=new xpc.sound;
	var prefix=document.documentURI.split("/").slice(0,-1).join("/")
	var url=ios.newURI(prefix+"/"+this.wavtable[c].filename,null,null)
	snd.play(url)
    },
    addmem:function(){
	var timer=new xpc.timer;
	var i=0;
	var numArray=this.buildNumArray()
	$("memFrame").value="M"
	this.memories.M=Number($("calcBox").value)
	numArray.push("M+")
	var timer=setTimeout(function(){
	    mikuCalc.speak(numArray[i])
	    i++
	    if(i==numArray.length) clearInterval(timer)
	    else{
		var timer=setTimeout(arguments.callee,mikuCalc.wavtable[numArray[i-1]].length)
	    }
	},0)
    },
    remember:function(){
	this.speak("MR")
	$("calcBox").value=this.memories.M
    },
    memClear:function(){
	this.speak("MC")
	this.memories.M=0
	$("memFrame").value=""
    }
}
addEventListener("load",function(){
    this.removeEventListener("load",arguments.callee,false)
    this.addEventListener("keypress",function(e){
	mikuCalc.input(String.fromCharCode(e.charCode))
    },false)
    for each(var i in document.getElementsByTagName("button")){
	if(!i.addEventListener) continue;
	i.addEventListener("command",function(){
	    mikuCalc.input(this.getAttribute("label"))
	},false)
    }
},false)
