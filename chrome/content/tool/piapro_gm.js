const Cc = Components.classes;
const Ci = Components.interfaces;
const TOOL_PROPERTY_PIAPROGM_ALERT = "extensions.vocalofx.tool_piaprogm_alert";
const PIAPRO_GMSCRIPTS = [
  "http://www7.atwiki.jp/piaprousers?cmd=upload&act=open&pageid=22&file=piapro_quickkeys86.user.js"
, "http://www7.atwiki.jp/piaprousers?cmd=upload&act=open&pageid=21&file=piapro_convsearch86.user.js"
, "http://www7.atwiki.jp/piaprousers?cmd=upload&act=open&pageid=13&file=piapro_quicktags86.user.js"
, "http://www7.atwiki.jp/piaprousers?cmd=upload&act=open&pageid=14&file=piapro_kurikenstar.user.js"
];
var pref;
var vocalofx_properties;
var install_failed;

function openPiaproGMInstall(){
	pref = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch);
	vocalofx_properties = document.getElementById("vocalofx-properties");
}

function doOK(){
	try{
		installAllScripts();
		alert(vocalofx_properties.getString("vocalofx.piapro_gm.complete"));
		pref.setBoolPref(TOOL_PROPERTY_PIAPROGM_ALERT, false);
	}
	catch(e){
		alert(vocalofx_properties.getString("vocalofx.piapro_gm.failed"));
	}
}

function doCancel(){
	pref.setBoolPref(TOOL_PROPERTY_PIAPROGM_ALERT, false);
}

function doExtra1(){
	close();
}

function installAllScripts(){
	for(var i = 0; i < PIAPRO_GMSCRIPTS.length; i++){
		install(PIAPRO_GMSCRIPTS[i]);
	}
}

function install(script){
	install_failed = true;
	var ioSvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
	var uri_ = ioSvc.newURI(script, null, null);
	var req_ = new XMLHttpRequest();
	req_.onload = function(){
		var config = Cc["@greasemonkey.mozdev.org/greasemonkey-service;1"].getService(Ci.gmIGreasemonkeyService).wrappedJSObject.config;
		var source = req_.responseText;
		var script = config.parse(source, uri_);
		var file = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties).get("TmpD", Ci.nsILocalFile);
		var base = script.name.replace(/[^A-Z0-9_]/gi, "").toLowerCase();
		file.append(base + ".user.js");
		file.createUnique(Ci.nsILocalFile.NORMAL_FILE_TYPE, 0640);
		var converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
		converter.charset = "UTF-8";
		source = converter.ConvertFromUnicode(source);
		var ws = Cc["@mozilla.org/network/file-output-stream;1"].createInstance(Ci.nsIFileOutputStream);
		ws.init(file, 0x02 | 0x08 | 0x20, 420, -1);
		ws.write(source, source.length);
		ws.close();
		script.setDownloadedFile(file);
		config.install(script);
		if(config._find(script) > -1){
			install_failed = false;
		}
	}
	req_.open("GET", uri_.spec, false);
	req_.send(null);
	if(install_failed){
		throw new Error();
	}
}
