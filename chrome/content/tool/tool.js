const TOOL_PROPERTY_POSITION = "extensions.vocalofx.tool.position";
const TOOL_PROPERTY_STARTUP = "extensions.vocalofx.tool.startup";
const TOOL_URL = "chrome://vocalofx/content/tool/";

var tool;

function openVocalofxTool(){
	tool = window.arguments[0];
	document.title = window.arguments[1];
	var toolhtml = document.getElementById("vocalofx-toolhtml");
	toolhtml.setAttribute("src", TOOL_URL + tool + ".html");
	toolhtml.style.width = window.innerWidth + "px";
	toolhtml.style.height = window.innerHeight + "px";
	window.opener.vocalofx.setVocalofxToolOpened(tool, true);
}

function closeVocalofxTool(){
	var pref = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
	var left = window.screenX;
	var top = window.screenY;
	var startup = false;
	if(window.opener.closed){
		var appver = pref.getCharPref("extensions.lastAppVersion");
		var verComp = Components.classes["@mozilla.org/xpcom/version-comparator;1"].getService(Components.interfaces.nsIVersionComparator);
		if(verComp.compare("3.5", appver) > 0){
			var offsetX = (window.outerWidth - window.innerWidth) / 2;
			var offsetY = window.outerHeight - window.innerHeight - offsetX;
			left -= offsetX;
			top -= offsetY;
		}
		startup = true;
	}
	else{
		window.opener.vocalofx.setVocalofxToolOpened(tool, false);
	}
	pref.setCharPref(TOOL_PROPERTY_POSITION + "." + tool, left + "," + top);
	pref.setBoolPref(TOOL_PROPERTY_STARTUP + "." + tool, startup);
}
