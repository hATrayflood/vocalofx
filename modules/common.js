var EXPORTED_SYMBOLS=["xpc"];

function svConstructor(contractID, interfaceName, initializer){
	var ccArgs = arguments;
	return (function(){
		var svc=Components.classes[contractID].getService()
		if(ccArgs.length>1){
			svc.QueryInterface(Components.interfaces[interfaceName]);
		}
		if(ccArgs.length>2){
			svc[initializer].apply(svc,arguments);
		}
		return svc;
	})
}

var xpc={
	verComp:Components.Constructor("@mozilla.org/xpcom/version-comparator;1", "nsIVersionComparator"),
	lclstr:Components.Constructor("@mozilla.org/pref-localizedstring;1","nsIPrefLocalizedString"),
	sptAry:Components.Constructor("@mozilla.org/supports-array;1","nsISupportsArray"),
	strbndSvc:svConstructor("@mozilla.org/intl/stringbundle;1","nsIStringBundleService"),
	file:Components.Constructor("@mozilla.org/file/local;1","nsILocalFile","initWithPath"),
	fp:Components.Constructor("@mozilla.org/filepicker;1","nsIFilePicker","init"),
	ioSvc:svConstructor("@mozilla.org/network/io-service;1","nsIIOService2"),
	prefSvc:svConstructor("@mozilla.org/preferences-service;1","nsIPrefService"),
	prefBrc2:svConstructor("@mozilla.org/preferences-service;1","nsIPrefBranch2"),
	dirSvc:svConstructor("@mozilla.org/file/directory_service;1","nsIProperties"),
	xferData:Components.Constructor("@mozilla.org/widget/transferable;1","nsITransferable"),
	obsSvc:svConstructor("@mozilla.org/observer-service;1","nsIObserverService"),
	conSvc:svConstructor("@mozilla.org/consoleservice;1","nsIConsoleService"),
	wm:svConstructor("@mozilla.org/appshell/window-mediator;1","nsIWindowMediator"),
	prompt:svConstructor("@mozilla.org/embedcomp/prompt-service;1","nsIPromptService"),
	fileHandler:function(){
		return (new xpc.ioSvc).getProtocolHandler("file").QueryInterface(Components.interfaces.nsIFileProtocolHandler)
	},
	ww:svConstructor("@mozilla.org/embedcomp/window-watcher;1","nsIWindowWatcher"),
	sound:Components.Constructor("@mozilla.org/sound;1","nsISound"),
	timer:Components.Constructor("@mozilla.org/timer;1","nsITimer"),
	clipboard:svConstructor("@mozilla.org/widget/clipboard;1","nsIClipboard"),
	supportsString:Components.Constructor("@mozilla.org/supports-string;1","nsISupportsString"),
	extMgr:svConstructor("@mozilla.org/extensions/manager;1","nsIExtensionManager"),
	rdfSvc:svConstructor("@mozilla.org/rdf/rdf-service;1","nsIRDFService"),
	dlMgr:svConstructor("@mozilla.org/download-manager;1","nsIDownloadManager")
};
