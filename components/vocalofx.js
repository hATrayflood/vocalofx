const Cc = Components.classes;
const Ci = Components.interfaces;
Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

function VocaloFxSplash(){}
VocaloFxSplash.prototype = {
	classID: Components.ID("{B77E342E-57D9-47da-9AD3-72B61E0B0D8C}"),
	classDescription: "VocaloFx Splash Screen",
	contractID: "@mozilla.org/commandlinehandler/general-startup;1?type=vocalofx_splash",
	_xpcom_categories: [{category: "command-line-handler", entry: "m-vocalofx_splash"}],
	QueryInterface: XPCOMUtils.generateQI([Ci.nsICommandLineHandler]),

	helpInfo: this.classDescription,
	handle: function(commandLine){
		var pref = Cc["@mozilla.org/preferences-service;1"].getService(Ci.nsIPrefBranch2);
		var mediator = Cc["@mozilla.org/appshell/window-mediator;1"].getService(Ci.nsIWindowMediator);
		var wins = mediator.getEnumerator(null);
		if(!wins.hasMoreElements() && pref.getCharPref("extensions.vocalofx.skin_splash.screen")){
			var watcher = Cc['@mozilla.org/embedcomp/window-watcher;1'].getService(Ci.nsIWindowWatcher);
			watcher.openWindow(null, "chrome://vocalofx/content/splash/splash.xul", "vocalofx-splash", "chrome,centerscreen,alwaysRaised=yes,titlebar=no,modal=yes", null);
		}
	}
};

function VocaloFxAboutVocaloFx(){}
VocaloFxAboutVocaloFx.prototype = {
	classID: Components.ID("{88C482A0-6DA2-4e20-A985-22D9C34C63A5}"),
	classDescription: "VocaloFx about:vocalofx",
	contractID: "@mozilla.org/network/protocol/about;1?what=vocalofx",
	QueryInterface: XPCOMUtils.generateQI([Ci.nsIAboutModule]),

	getURIFlags: function(aURI){
		return Ci.nsIAboutModule.ALLOW_SCRIPT | Ci.nsIAboutModule.URI_SAFE_FOR_UNTRUSTED_CONTENT;
	}
	,
	newChannel: function(aURI){
		var ios = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
		var channel = ios.newChannel("chrome://vocalofx/content/about/readme.html", null, null);
		channel.originalURI = aURI;
		return channel;
	}
};

function VocaloFxAboutIcecream(){}
VocaloFxAboutIcecream.prototype = {
	classID: Components.ID("{176D7340-39A5-499f-B3F2-C3C2A7A99FB4}"),
	classDescription: "VocaloFx about:icecream",
	contractID: "@mozilla.org/network/protocol/about;1?what=icecream",
	QueryInterface: XPCOMUtils.generateQI([Ci.nsIAboutModule]),

	getURIFlags: function(aURI){
		return Ci.nsIAboutModule.ALLOW_SCRIPT | Ci.nsIAboutModule.URI_SAFE_FOR_UNTRUSTED_CONTENT;
	}
	,
	newChannel: function(aURI){
		var ios = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
		var channel = ios.newChannel("chrome://vocalofx/content/about/icecream.html", null, null);
		channel.originalURI = aURI;
		return channel;
	}
};

function VocaloFxAboutOnecup(){}
VocaloFxAboutOnecup.prototype = {
	classID: Components.ID("{04802179-A72F-4a4d-8708-9BDF4727CD9D}"),
	classDescription: "VocaloFx about:onecup",
	contractID: "@mozilla.org/network/protocol/about;1?what=onecup",
	QueryInterface: XPCOMUtils.generateQI([Ci.nsIAboutModule]),

	getURIFlags: function(aURI){
		return Ci.nsIAboutModule.ALLOW_SCRIPT | Ci.nsIAboutModule.URI_SAFE_FOR_UNTRUSTED_CONTENT;
	}
	,
	newChannel: function(aURI){
		var ios = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
		var channel = ios.newChannel("chrome://vocalofx/content/about/onecup.html", null, null);
		channel.originalURI = aURI;
		return channel;
	}
};

function NSGetModule(compMgr, fileSpec){
	return XPCOMUtils.generateModule([VocaloFxSplash, VocaloFxAboutVocaloFx, VocaloFxAboutIcecream, VocaloFxAboutOnecup]);
}
