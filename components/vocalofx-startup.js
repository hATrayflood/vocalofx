const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;
Cu.import("resource://gre/modules/XPCOMUtils.jsm");

function VocaloFxStartup(){}
VocaloFxStartup.prototype = {
	classID           : Components.ID("{C5752C51-3062-42db-A5F8-8CF9920B4757}"),
	classDescription  : "VocaloFx Startup",
	contractID        : "@rayflood.org/vocalofx-startup;1",
	_xpcom_categories : [{category: "app-startup", service: true}],
	QueryInterface    : XPCOMUtils.generateQI([Ci.nsIObserver]),
	helpInfo          : this.classDescription,

	APP_STARTUP      : "app-startup",
	FINAL_UI_STARTUP : "final-ui-startup",
	QUIT_APPLICATION : "quit-application",

	ENABLED_ITEMS    : "extensions.enabledItems",
	DEFAULT_THEME    : "{972ce4c6-7e08-4474-a285-3208198ce6fd}",
	MOZ_LWTHEME_USED : "lightweightThemes.isThemeSelected",
	GREASEMONKEY_ID  : "{e4a8a97b-f2ed-450b-b12d-ee082ba24781}",

	VOCALOFX_PROPERTY_VERSION  : "extensions.vocalofx.version",
	SKIN_PROPERTY_IMAGE        : "extensions.vocalofx.skin_image",
	SKIN_PROPERTY_PALLET       : "extensions.vocalofx.skin_pallet",
	SKIN_PROPERTY_POSITION     : "extensions.vocalofx.skin_position",
	SKIN_PROPERTY_CATEGORY     : "extensions.vocalofx.skin_category",
	SKIN_PROPERTY_USE_ALLPARET : "extensions.vocalofx.skin_use_allparet",
	SKIN_SELECT_RANDOM         : "extensions.vocalofx.skin_select_random",
	ALERT_ADDON                : "extensions.vocalofx.alert_addon.",
	ALERT_THEME                : "extensions.vocalofx.skin_theme_alert",
	ALERT_PIAPROGM             : "extensions.vocalofx.tool_piaprogm_alert",

	observe : function(aSubject, aTopic, aData){
		switch(aTopic){
		case this.APP_STARTUP:
			this.constructor();
			this.obsSvc.addObserver(this, this.FINAL_UI_STARTUP, false);
			this.obsSvc.addObserver(this, this.QUIT_APPLICATION, false);
		break;
		case this.FINAL_UI_STARTUP:
			this.obsSvc.removeObserver(this, this.FINAL_UI_STARTUP);
			this.init();
			this.selectSkinByRandom();
			this.alert();
			this.piaprogm();
		break;
		case this.QUIT_APPLICATION:
			this.obsSvc.removeObserver(this, this.QUIT_APPLICATION);
		break;
		}
	}
	,
	constructor : function(){
		var ios=Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
		var resProt=ios.getProtocolHandler("resource").QueryInterface(Ci.nsIResProtocolHandler);
		var modulesDir=__LOCATION__.parent.parent;
		modulesDir.append("modules");
		resProt.setSubstitution("p4v-jsm",ios.newFileURI(modulesDir));
		Cu.import("resource://p4v-jsm/common.js");

		this.obsSvc = new xpc.obsSvc;
		this.prefSvc = new xpc.prefSvc;
		this.prefBrc2 = new xpc.prefBrc2;
		this.extMgr = new xpc.extMgr;
		this.strbndSvc = new xpc.strbndSvc;
		this.verComp = new xpc.verComp;
		this.ww = new xpc.ww;
		this.skin_properties = this.strbndSvc.createBundle("chrome://vocalofx/content/skin/skin.properties");
	}
	,
	init : function(){
		this.enabledItems = {};

		var items = this.prefBrc2.getCharPref(this.ENABLED_ITEMS).split(",");
		for(var i = 0; i < items.length; i++){
			var item = items[i].split(":");
			this.enabledItems[item[0]] = item[1];
		}

		var addon = this.extMgr.getItemForID("vocalofx@rayflood.org");
		var version = "0.1pre";
		try{
			version = this.prefBrc2.getCharPref(this.VOCALOFX_PROPERTY_VERSION);
		}
		catch(e){
			try{
				this.prefBrc2.getCharPref(this.SKIN_PROPERTY_CATEGORY);
			}
			catch(e){
				version = "0.0";
			}
		}
		if(version == addon.version){
			return;
		}

		this.prefBrc2.setCharPref(this.VOCALOFX_PROPERTY_VERSION, addon.version);
		if(version == "0.0"){
			this.initVocalofxPref();
		}
		else{
			this.resetVocalofxSkin();
		}
		if(this.verComp.compare("1.0", version) > 0){
			this.ww.openWindow(null, "chrome://vocalofx/content/splash/splash_alert.xul", "vocalofx-splash-alert", "chrome,centerscreen,alwaysRaised=yes,modal=yes", null);
		}
	}
	,
/* 0.4
user_pref("extensions.vocalofx.skin_category", "rinlen");
user_pref("extensions.vocalofx.skin_image", "chrome://vocalofx/content/skin/zasikiusa_rinlen.png");
user_pref("extensions.vocalofx.skin_pallet", "rinlen3");
user_pref("extensions.vocalofx.skin_pallet.miku", "miku3"); // default
user_pref("extensions.vocalofx.skin_pallet.rinlen", "rinlen3"); // default
user_pref("extensions.vocalofx.skin_pallet_alert", true); // default
user_pref("extensions.vocalofx.skin_position", "250,2");
user_pref("extensions.vocalofx.skin_theme_alert", true); // default
user_pref("extensions.vocalofx.tool.position.fullmix", "8,228");
user_pref("extensions.vocalofx.tool.startup.fullmix", false);
user_pref("extensions.vocalofx.tool_piaprogm_alert", true); // default
user_pref("extensions.vocalofx.version", "0.4");
*/

/* 0.1pre3 - 0.3
user_pref("extensions.vocalofx.skin_category", "miku");
user_pref("extensions.vocalofx.skin_image", "chrome://vocalofx/content/skin/nagamon_miku.png");
user_pref("extensions.vocalofx.skin_pallet", "miku");
user_pref("extensions.vocalofx.skin_pallet.miku", "miku");
user_pref("extensions.vocalofx.skin_pallet_alert", false);
user_pref("extensions.vocalofx.skin_position", "160,2");
user_pref("extensions.vocalofx.tool.position.fullmiku", "1033,272");
user_pref("extensions.vocalofx.tool.startup.fullmiku", true);
user_pref("extensions.vocalofx.version", "0.1pre3");
*/
	initVocalofxPref : function(){
		var srchEngine = new xpc.lclstr;
		srchEngine.data = "ピアプロ音楽検索";
		prefBrc2.setComplexValue("browser.search.selectedEngine", Ci.nsIPrefLocalizedString, srchEngine);
	}
	,
/* 0.1pre2
user_pref("extensions.vocalofx.skin_category", "miku");
user_pref("extensions.vocalofx.skin_image", "chrome://vocalofx/content/skin/nagamon_miku.png");
user_pref("extensions.vocalofx.skin_pallet", "miku");
user_pref("extensions.vocalofx.skin_pallet.miku", "miku");
user_pref("extensions.vocalofx.skin_position", "160,2");
user_pref("extensions.vocalofx.tool.position.fullmiku", "1033,272");
user_pref("extensions.vocalofx.tool.startup.fullmiku", true);
user_pref("extensions.vocalofx.version", "0.1pre2");
*/

/* 0.1pre
user_pref("extensions.vocalofx.skin_category", "miku");
user_pref("extensions.vocalofx.skin_image", "chrome://vocalofx/content/skin/nagamon_miku.png");
user_pref("extensions.vocalofx.skin_position", "95% -10%");
user_pref("extensions.vocalofx.tool.position.fullmiku", "425,256");
user_pref("extensions.vocalofx.tool.startup.fullmiku", false);
*/
	alert : function(){
		var installedAddons = new xpc.sptAry;

		var addons = this.prefSvc.getBranch(this.ALERT_ADDON).getChildList("", {});
		for(var i = 0; i < addons.length; i++){
			if(!this.prefBrc2.getBoolPref(this.ALERT_ADDON + addons[i])){
				continue;
			}
			if(this.enabledItems[addons[i]]){
				var string = new xpc.supportsString;
				string.data = addons[i];
				installedAddons.AppendElement(string);
			}
		}

		if(this.prefBrc2.getBoolPref(this.ALERT_THEME) && (this.prefBrc2.getCharPref("general.skins.selectedSkin") != "classic/1.0" || this.isLWThemeUsed())){
			var string = new xpc.supportsString;
			string.data = this.DEFAULT_THEME;
			installedAddons.AppendElement(string);
		}

		if(installedAddons.Count()){
			this.ww.openWindow(null, "chrome://vocalofx/content/alert.xul", "vocalofx-alert", "chrome,centerscreen,alwaysRaised=yes,modal=yes", installedAddons);
		}
	}
	,
	isLWThemeUsed : function(){
		var lwt_used;
		try{
			lwt_used = this.prefBrc2.getBoolPref(this.MOZ_LWTHEME_USED);
		}
		catch(e){
			lwt_used = false;
		}
		return lwt_used;
	}
	,
	piaprogm : function(){
		if(this.enabledItems[this.GREASEMONKEY_ID] && this.prefBrc2.getBoolPref(this.ALERT_PIAPROGM)){
			this.ww.openWindow(null, "chrome://vocalofx/content/tool/piapro_gm.xul", "piaprogm-alert", "chrome,centerscreen,alwaysRaised=yes,modal=yes", null);
		}
	}
	,
	selectSkinByRandom : function(){
		if(!this.prefBrc2.getBoolPref(this.SKIN_SELECT_RANDOM)){
			return;
		}

		try{
			var skins = this.skin_properties.GetStringFromName("skinmenu").split(",");
			var skin_index = Math.floor(Math.random() * skins.length);
			var category = this.skin_properties.GetStringFromName(skins[skin_index] + ".category");
			var pallets;
			if(category == "piapro" || this.prefBrc2.getBoolPref(this.SKIN_PROPERTY_USE_ALLPARET)){
				pallets = this.skin_properties.GetStringFromName("palletmenu").split(",");
			}
			else{
				pallets = this.skin_properties.GetStringFromName("palletmenu." + category).split(",");
			}
			var pallet_index = Math.floor(Math.random() * (pallets.length - 1));
			this.selectVocalofxSkin(skins[skin_index], pallets[pallet_index]);
		}
		catch(e){
			// nop
		}
	}
	,
	resetVocalofxSkin : function(){
		try{
			var image = this.prefBrc2.getCharPref(this.SKIN_PROPERTY_IMAGE);
			var token = image.split("/");
			var imagefile = token[token.length - 1];
			var skin = imagefile.substr(0, imagefile.lastIndexOf(".png"));
			var pallet = this.prefBrc2.getCharPref(this.SKIN_PROPERTY_PALLET);
			this.selectVocalofxSkin(skin, pallet);
		}
		catch(e){
			// nop
		}
	}
	,
	selectVocalofxSkin : function(skin, pallet){
		try{
			var position = this.skin_properties.GetStringFromName(skin + ".position");
			var category = this.skin_properties.GetStringFromName(skin + ".category");
			this.prefBrc2.setCharPref(this.SKIN_PROPERTY_IMAGE, "chrome://vocalofx/content/skin/" + skin + ".png");
			this.prefBrc2.setCharPref(this.SKIN_PROPERTY_POSITION, position);
			this.prefBrc2.setCharPref(this.SKIN_PROPERTY_CATEGORY, category);
			this.prefBrc2.setCharPref(this.SKIN_PROPERTY_PALLET + "." + category, pallet);
			this.prefBrc2.setCharPref(this.SKIN_PROPERTY_PALLET, pallet);
		}
		catch(e){
			// nop
		}
	}
};

function NSGetModule(compMgr, fileSpec){
	return XPCOMUtils.generateModule([VocaloFxStartup]);
}
